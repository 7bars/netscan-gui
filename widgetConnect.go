package main

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
)

var (
	treeWidget          *widgets.QTreeWidget
	rootNode            *widgets.QTreeWidgetItem
	currentSlave        []*widgets.QTreeWidgetItem
	currentAvailaleHost []*widgets.QTreeWidgetItem
	scanSelectBtn       *widgets.QPushButton
	scanAllBtn          *widgets.QPushButton
	slave               []Slave1
)

type Slave1 struct {
	IP   string `json:"IP"`
	Port string `json:"port"`
}

type ListIP struct {
	IP []string
}

type SlaveScan struct {
	IP     string `json:"IP"`
	Port   string `json:"port"`
	Target string `json:"target"`
}

type Vulnerability struct {
	Service []SVC `json:"service"`
}

type SVC struct {
	Port     string    `json:"port"`
	Name     string    `json:"name"`
	Version  string    `json:"version"`
	Problems []Problem `json:"problems"`
}

type Problem struct {
	Description string `json:"description"`
	Score       string `json:"score"`
}

func widgetConnect() {
	widget := widgets.NewQWidget(nil, 0)
	widget.SetLayout(widgets.NewQVBoxLayout())

	connectBtn := widgets.NewQPushButton2("New connect", nil)
	connectBtn.ConnectClicked(func(checked bool) {
		// connectBtnEvent(checked)
	})
	scanSelectBtn = widgets.NewQPushButton2("Scan selected", nil)
	scanSelectBtn.SetEnabled(false)
	scanSelectBtn.ConnectClicked(func(checked bool) {
		scanSelectBtnEvent(checked)
	})

	scanAllBtn = widgets.NewQPushButton2("Scan all", nil)
	scanAllBtn.SetEnabled(false)
	scanAllBtn.ConnectClicked(func(checked bool) {
		scanAllBtnEvent(checked)
	})

	//Tree Widget
	treeWidget = widgets.NewQTreeWidget(nil)
	treeWidget.SetHeaderLabels([]string{"IP", "STATE"})
	rootNode = treeWidget.InvisibleRootItem()

	treeWidget.ExpandAll()

	widget.Layout().AddWidget(connectBtn)
	widget.Layout().AddWidget(treeWidget)
	widget.Layout().AddWidget(scanAllBtn)
	widget.Layout().AddWidget(scanSelectBtn)

	widget.SetFixedSize2(frame.Width/offsetX, frame.Height)
	mainWindow.Layout().AddWidget(widget)
	// report()
	go fillTreeWidget()
}

func fillTreeWidget() {

	connectRequest(MasterIP, MasterPort, &slave)

	currentSlave = make([]*widgets.QTreeWidgetItem, len(slave))

	for i := range slave {

		go fillTreeChild(&slave[i], i)
		// go tts(i)
	}
}

func fillTreeChild(slave *Slave1, pos int) {
	var listIP ListIP

	currentSlave[pos] = widgets.NewQTreeWidgetItem2([]string{slave.IP, "Wait..."}, 0)
	rootNode.AddChild(currentSlave[pos])

	err := hostsRequest(MasterIP, MasterPort, &slave, &listIP)
	if err != nil {
		currentSlave[pos] = widgets.NewQTreeWidgetItem2([]string{slave.IP, "Error"}, 0)
	} else {
		currentAvailaleHost = make([]*widgets.QTreeWidgetItem, len(listIP.IP))
		for i := range listIP.IP {
			currentAvailaleHost[i] = widgets.NewQTreeWidgetItem2([]string{listIP.IP[i], "Available"}, 0)
			currentAvailaleHost[i].SetCheckState(0, core.Qt__Unchecked)
			currentSlave[pos].AddChild(currentAvailaleHost[i])
		}
		currentSlave[pos].SetText(1, "Ready")
	}
	scanAllBtn.SetEnabled(true)
	scanSelectBtn.SetEnabled(true)
}

func scanSelectBtnEvent(checked bool) {
	var vulnerability Vulnerability
	for i := range currentAvailaleHost {
		if currentAvailaleHost[i].CheckState(0) == core.Qt__Checked {
			scanRequest(MasterIP, MasterPort, &SlaveScan{slave[0].IP, slave[0].Port, currentAvailaleHost[i].Text(0)}, &vulnerability)
			report(currentAvailaleHost[i].Text(0), &vulnerability)
		}
	}
}

func scanAllBtnEvent(checked bool) {
	for i := range currentAvailaleHost {
		if currentAvailaleHost[i].CheckState(0) == core.Qt__Unchecked {
			currentAvailaleHost[i].SetCheckState(0, core.Qt__Checked)
		}
	}
}

func report(ip string, vul *Vulnerability) {
	rep := " \nInformation about: " + ip + "\n"
	for i := range vul.Service {
		rep += "	Name service: " + vul.Service[i].Name + "\n"
		if vul.Service[i].Version != "" {
			rep += "	Version: " + vul.Service[i].Version + "\n"
		}
		rep += "	Port: " + vul.Service[i].Port + "\n"

		if len(vul.Service[i].Problems) != 0 {
			rep += "   Founded vulnerability:\n"
			for j := range vul.Service[i].Problems {
				rep += "		Description: " + vul.Service[i].Problems[j].Description + "\n"
				rep += "		Score: " + vul.Service[i].Problems[j].Score + "\n"
			}

		}
	}

	txtBw := widgets.NewQTextBrowser(nil)
	txtBw.SetText(rep)
	tabWidget.AddTab(txtBw, ip)

	// txt.SetText(rep)
}
