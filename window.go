package main

import (
	"os"

	"github.com/therecipe/qt/widgets"
)

//Window contains Width and Height
type Window struct {
	Width  int
	Height int
}

var (
	mainWindow *widgets.QMainWindow
	frame      Window
)

const (
	//it's mean that how to be offset widget connect and tab
	//1 to 4 default
	offsetX = 4
)

func setupWindow() {
	widgets.NewQApplication(len(os.Args), os.Args)
	mainWindow = widgets.NewQMainWindow(nil, 0)
	mainWindow.SetLayout(widgets.NewQVBoxLayout())
	mainWindow.SetWindowTitle("netscan")

	// frame.Width = mainWindow.Width()
	// frame.Height = mainWindow.Height()
	frame.Width = 1920
	frame.Height = 1000
	mainWindow.ShowMaximized()

	// mainWindow.ShowMaximized()
	menuBar()

	widgetTab()
	widgetConnect()
	widgets.QApplication_Exec()
}
