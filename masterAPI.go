package main

import (
	"bytes"
	"encoding/json"
	"net/http"
)

func checkRequest() {

}

func connectRequest(ip string, port string, output interface{}) {
	mainRequest(ip, port, "/connect", nil, output)
}

func hostsRequest(ip string, port string, input interface{}, output interface{}) error {
	return mainRequest(ip, port, "/hosts", input, output)
}

func scanRequest(ip string, port string, input interface{}, output interface{}) {
	mainRequest(ip, port, "/scan", input, output)
}

func mainRequest(ip string, port string, call string, parameters interface{}, target interface{}) error {

	// if parameters != nil {
	param, err := json.Marshal(parameters)
	if err != nil {
		return err
	}
	// } else {
	// 	var param []byte
	// }

	url := "http://" + ip + ":" + port + call
	req, err := http.NewRequest("GET", url, bytes.NewBuffer(param))
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return json.NewDecoder(resp.Body).Decode(target)
}
