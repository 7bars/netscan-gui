package main

import (
	"github.com/therecipe/qt/widgets"
)

var (
	tabWidget *widgets.QTabWidget
	txt       *widgets.QTextBrowser
)

/*
Common view:
|==============================|
||tab1/tab2/............./tabN||
||                            ||
||                            ||
||                            ||
||                            ||
||____________________________||
|____Button1_______Button2_____|
*/
func widgetTab() {
	widget := widgets.NewQWidget(nil, 0)
	widget.SetLayout(widgets.NewQVBoxLayout())

	buttonWidget := widgets.NewQWidget(nil, 0)
	buttonWidget.SetLayout(widgets.NewQHBoxLayout())

	unknownBtn1 := widgets.NewQPushButton2("Scan all", nil)
	unknownBtn2 := widgets.NewQPushButton2("Scan all", nil)

	buttonWidget.Layout().AddWidget(unknownBtn1)
	buttonWidget.Layout().AddWidget(unknownBtn2)

	tabWidget = widgets.NewQTabWidget(mainWindow)
	// tabWidget.SetTabsClosable(true)
	txt = widgets.NewQTextBrowser(nil)
	txt.SetText("\n Here will be information about your action.")
	// tab1 := widgets.NewQTableView(nil)
	// tabWidget.AddTab(tab1, "SMTG")
	// txt.SetText("Some thing")
	tabWidget.AddTab(txt, "Welcome")

	// tabWidget.AddTab(widgets.NewQTextBrowser(nil), "currentIP")
	// tabWidget.AddTab(widgets.NewQTextBrowser(nil), "currentIP")
	// tabWidget.AddTab(widgets.NewQTextBrowser(nil), "currentIP")
	// tabWidget.AddTab(widgets.NewQTextBrowser(nil), "currentIP")
	// tabWidget.AddTab(widgets.NewQTextBrowser(nil), "currentIP")
	// tabWidget.AddTab(widgets.NewQTextBrowser(nil), "currentIP")
	// tabWidget.AddTab(widgets.NewQTextBrowser(nil), "currentIP")
	// tabWidget.AddTab(widgets.NewQTextBrowser(nil), "currentIP")

	widget.Layout().AddWidget(tabWidget)
	widget.Layout().AddWidget(buttonWidget)
	widget.SetFixedSize2(frame.Width-frame.Width/offsetX, frame.Height)
	widget.Move2(frame.Width/offsetX, 0)
	mainWindow.Layout().AddWidget(widget)
}
